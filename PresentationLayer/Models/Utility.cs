﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using ModelLayer.Model;
using BusinessLayer;

namespace PresentationLayer.Models
{
    public class Utility
    {
        //SET THE DEFAULT TEXT THAT DISPLAYS ON THE <INPUT>, <SELECT> LIST B/4 A SELECTION PROCESS OCCURS
        public const string ID = "Id";
        public const string NAME = "Name";
        public const string VALUE = "Value";
        public const string TEXT = "Text";
        public const string Select = "-- Select --";
        public const string DEFAULT_AVATAR = "/Content/Images/default_user_icon.png";
        public const string SelectDepartment = "-- Select Department --";
        public const string SelectAdmissiontype = "-- Select Admission Type --";
        public const string SelectSession = "-- Select Session --";
        public const string SelectSemester = "-- Select Semester --";
        public const string SelectLevel = "-- Select Level --";
        public const string SelectProgramme = "-- Select Programme --";
        public const string SelectCourse = "-- Select Course --";
        public const string SelectPaymentMode = "-- Select Payment Mode --";
        public const string SelectState = "-- Select State --";
        public const string SelectGender = "-- Select Gender --";
        public const string SelectBank = "-- Select Bank --";
        public const string SelectTitle = "-- Select Title --";
        public const string SelectRelationship = "-- Select Relationship --";
        public const string SelectRetirementReason = "-- Select Retirement Reason --";
        public const string SelectMaritalStatus = "-- Select Marital Status --";

        public static void BindDropdownItem<T>(DropDownList dropDownList, T items, string dataValueField, string dataTextField)
        {
            dropDownList.Items.Clear();

            dropDownList.DataValueField = dataValueField;
            dropDownList.DataTextField = dataTextField;

            dropDownList.DataSource = items;
            dropDownList.DataBind();
        }

        public static List<Value> CreateYearListFrom(int startYear)
        {
            List<Value> years = new List<Value>();

            try
            {
                for (int i = startYear; i <= DateTime.Now.Year; i++)
                {
                    Value value = new Value();
                    value.Id = i;
                    value.Name = i.ToString();
                    years.Add(value);
                }
                return years;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<Value> CreateYearListFrom(int startYear, int endYear)
        {
            List<Value> years = new List<Value>();
            try
            {
                for (int i = startYear; i <= endYear; i++)
                {
                    Value value = new Value();
                    value.Id = i;
                    value.Name = i.ToString();
                    years.Add(value);
                }
                return years;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<Value> CreateNumberListFrom(int startValue, int endValue)
        {
            List<Value> values = new List<Value>();
            try
            {
                for (int i = startValue; i <= endValue; i++)
                {
                    Value value = new Value();
                    value.Id = i;
                    value.Name = i.ToString();
                    values.Add(value);
                }
                return values;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<Value> GetMonthsInYear()
        {
            List<Value> values = new List<Value>();
            try
            {
                string[] names = DateTimeFormatInfo.CurrentInfo.MonthNames;
                for (int i = 0; i <= names.Length; i++)
                {
                    Value value = new Value();
                    value.Id = i + 1;
                    value.Name = names[i];
                    values.Add(value);
                }
                return values;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static List<SelectListItem> PopulateAllStatesItems()
        {
            try
            {
                StatesLogic statesLogic = new StatesLogic();
                List<States> states = statesLogic.GetAll().OrderByDescending(s => s.State).ToList();

                if (states == null || states.Count() <= 0)
                {
                    return new List<SelectListItem>();
                }

                List<SelectListItem> selectListItem = new List<SelectListItem>();
                //Set the default text that serves as a placeholder for the <input>
                SelectListItem list = new SelectListItem();
                list.Value = "";
                list.Text = SelectState;

                selectListItem.Add(list);

                foreach (States state in states)
                {
                    SelectListItem selectList = new SelectListItem();
                    selectList.Value = state.Id.ToString();
                    selectList.Text = state.State;

                    selectListItem.Add(selectList);
                }

                return selectListItem;

            }
            catch (Exception ex) { throw ex; }
        }

        public static List<SelectListItem> PopulateAllBankItems()
        {
            try
            {
                BankLogic bankLogic = new BankLogic();
                List<Bank> banks = bankLogic.GetAll().OrderByDescending(b => b.BankName).ToList();

                if (banks == null || banks.Count() <= 0)
                {
                    return new List<SelectListItem>();//return an empty array
                }

                List<SelectListItem> selectItemList = new List<SelectListItem>();
                //set defaults
                SelectListItem list = new SelectListItem();
                list.Value = "";
                list.Text = SelectBank;
                selectItemList.Add(list);

                foreach (Bank bank in banks)
                {
                    SelectListItem selectList = new SelectListItem();
                    selectList.Value = bank.Id.ToString();
                    selectList.Text = bank.BankName;

                    selectItemList.Add(selectList);
                }
                return selectItemList;
            }
            catch (Exception ex) { throw ex; }
        }

        public static List<SelectListItem> PopulateAllGenderListItems()
        {
            try
            {
                GenderLogic genderLogic = new GenderLogic();
                List<Gender> genders = genderLogic.GetAll().ToList();

                if (genders == null || genders.Count() <= 0)
                {
                    return new List<SelectListItem>();
                }

                List<SelectListItem> selectListItems = new List<SelectListItem>();
                foreach (Gender gender in genders)
                {
                    SelectListItem selectItem = new SelectListItem();
                    selectItem.Value = gender.Id.ToString();
                    selectItem.Text = gender.GenderType;

                    selectListItems.Add(selectItem);
                }

                return selectListItems;
            }
            catch (Exception ex) { throw ex; }
        }

        public static List<SelectListItem> PopulateAllTitleListItems()
        {
            try
            {
                TitleLogic titleLogic = new TitleLogic();
                List<Titles> titles = titleLogic.GetAll().OrderByDescending(t => t.Title).ToList();

                if (titles == null || titles.Count() <= 0)
                {
                    return new List<SelectListItem>();
                }

                List<SelectListItem> selectItemList = new List<SelectListItem>();
                SelectListItem list = new SelectListItem();
                list.Value = "";
                list.Text = SelectTitle;

                selectItemList.Add(list);

                foreach (Titles title in titles)
                {
                    SelectListItem selectItem = new SelectListItem();
                    selectItem.Value = title.Id.ToString();
                    selectItem.Text = title.Title;

                    selectItemList.Add(selectItem);
                }

                return selectItemList;
            }
            catch (Exception ex) { throw ex; }
        }

        public static List<SelectListItem> PopulateAllRelationshipItems()
        {
            try
            {
                RelationshipLogic relationshipLogic = new RelationshipLogic();
                List<Relationships> relationshipList = relationshipLogic.GetAll().OrderByDescending(r => r.Relationship).ToList();

                if (relationshipList == null || relationshipList.Count() <= 0)
                {
                    return new List<SelectListItem>();
                }

                List<SelectListItem> selectListItem = new List<SelectListItem>();
                SelectListItem list = new SelectListItem();
                list.Value = "";
                list.Text = SelectRelationship;

                selectListItem.Add(list);
                foreach (Relationships relationship in relationshipList)
                {
                    SelectListItem selectList = new SelectListItem();
                    selectList.Value = relationship.Id.ToString();
                    selectList.Text = relationship.Relationship;

                    selectListItem.Add(selectList);
                }

                return selectListItem;
            }
            catch (Exception ex) { throw ex; }
        }

        public static List<SelectListItem> PopulateAllRetirementReasonItems()
        {
            try
            {
                RetirementReasonLogic retirementReasonLogic = new RetirementReasonLogic();
                List<RetirementReason> retirementReasons = retirementReasonLogic.GetAll().OrderByDescending(r => r.Reason).ToList();

                if (retirementReasons == null || retirementReasons.Count() <= 0)
                {
                    return new List<SelectListItem>();
                }

                List<SelectListItem> selectListItem = new List<SelectListItem>();
                SelectListItem list = new SelectListItem();
                list.Value = "";
                list.Text = SelectRetirementReason;

                selectListItem.Add(list);
                foreach (RetirementReason retirementReason in retirementReasons)
                {
                    SelectListItem selectItem = new SelectListItem();
                    selectItem.Value = retirementReason.Id.ToString();
                    selectItem.Text = retirementReason.Reason;

                    selectListItem.Add(selectItem);
                }
                return selectListItem;
            }
            catch (Exception ex) { throw ex; }
        }

        public static List<SelectListItem> PopulateAllMartialStatusItems()
        {
            try
            {
                MaritalStatusLogic maritalStatusLogic = new MaritalStatusLogic();
                List<MaritalStatus> maritalStatuses = maritalStatusLogic.GetAll().OrderByDescending(m => m.Status).ToList();

                if (maritalStatuses == null || maritalStatuses.Count() <= 0)
                {
                    return new List<SelectListItem>();
                }

                List<SelectListItem> selectListItem = new List<SelectListItem>();
                SelectListItem list = new SelectListItem();
                list.Value = "";
                list.Text = SelectMaritalStatus;

                selectListItem.Add(list);

                foreach (MaritalStatus maritalStatus in maritalStatuses)
                {
                    SelectListItem item = new SelectListItem();
                    item.Value = maritalStatus.Id.ToString();
                    item.Text = maritalStatus.Status;

                    selectListItem.Add(item);
                }
                return selectListItem;
            }
            catch (Exception ex) { throw ex; }
        }

        public static List<Value> GetNumberOfDaysInMonth(Value month, Value year)
        {
            try
            {
                List<Value> days = CreateNumberListFrom(1, DateTime.DaysInMonth(year.Id, month.Id));
                return days;
            }
            catch (Exception ex) { throw ex; }
        }

        public static bool IsDateInTheFuture(DateTime date)
        {
            try
            {
                return ((DateTime.Now - date).Days <= 0);
            }
            catch (Exception ex) { throw ex; }
        }

        public static bool IsStartDateGreaterThanEndDate(DateTime startDate, DateTime endDate)
        {
            try
            {
                return ((endDate - startDate).Days <= 0);
            }
            catch (Exception ex) { throw ex; }
        }

        public static bool IsDateOutOfRange(DateTime startDate, DateTime endDate, int noOfDays)
        {
            try
            {
                return ((endDate - startDate).Days < noOfDays);
            }
            catch (Exception ex) { throw ex; }
        }

        public static string Encrypt(string encryptData)
        {
            string data = "";
            try
            {
                string charData = "";
                string conChar = "";
                for (int i = 0; i < encryptData.Length; i++)
                {
                    charData = Convert.ToString(encryptData.Substring(i, 1));
                    conChar = char.ConvertFromUtf32(char.ConvertToUtf32(charData, 0) + 128);
                    data += conChar;
                }
            }
            catch (Exception)
            {
                data = "1";
                return data;
            }
            return data;
        }

        public static string Decrypt(string encryptData)
        {
            string data = "";
            try
            {
                string CharData = "";
                string Conchar = "";
                for (int i = 0; i < encryptData.Length; i++)
                {
                    CharData = Convert.ToString(encryptData.Substring(i, 1));
                    Conchar = char.ConvertFromUtf32(char.ConvertToUtf32(CharData, 0) - 128);
                    data += Conchar;
                }
            }
            catch (Exception)
            {
                data = "1";
                return data;
            }
            return data;
        }

        public static Pensioner GetPensioner(int pensionerId)
        {
            try
            {
                PensionerLogic pensionerLogic = new PensionerLogic();
                return pensionerLogic.GetBy(pensionerId);
            }
            catch (Exception ex) { throw ex; }
        }

        //public static Student GetStudent(string MatricNumber)
        //{
        //    try
        //    {
        //        StudentLogic studentLogic = new StudentLogic();
        //        return studentLogic.GetBy(MatricNumber);
        //    }
        //    catch (Exception ex)
        //    {

        //        throw ex;
        //    }
        //}


    }
}