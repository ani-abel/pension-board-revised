﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ModelLayer.Model;
using System.Web.Mvc;
using BusinessLayer;
using System.IO;
using System.Text.RegularExpressions;

namespace PresentationLayer.Controllers
{
    public class BaseController : Controller
    {
        protected void SetMessage(string message, Message.Category messageType)
        {
            Message msg = new Message(message, (int)messageType);
            TempData["Message"] = msg;
        }

        protected void SetHttpCookies(string cookieName, string value)//Set cookies that expire in 7 days
        {
            HttpCookie cookie = new HttpCookie(cookieName);
            cookie.Expires = DateTime.Now.AddDays(7);
            cookie.Value = value;
            Response.Cookies.Add(cookie);

            this.Response.Cookies.Add(cookie);
        }

        protected int GetHttpCookiesAsInt(string cookieName)
        {
            int cookieValue;
            Int32.TryParse(Request.Cookies[cookieName].Value, out cookieValue);
            return cookieValue;
        }

        protected bool DoesCookieExist(string cookieName)
        {
            return Request.Cookies[cookieName] != null;
        }

        protected string GetHttpCookie(string cookieName)
        {
            return (DoesCookieExist(cookieName) ? Request.Cookies[cookieName].Value : null);
        }

        protected User GetLoggedInUser(string username, string password)
        {
            try
            {
                UserLogic userLogic = new UserLogic();
                return userLogic.GetUser(username, password);
            }
            catch (Exception ex)
            {
                SetMessage("Error Occurred! " + ex.Message, Message.Category.Error);
            }
            return null;
        }

        private bool ValidateFile(HttpPostedFileBase File)
        {
            return (File.ContentLength < 3000000 && Regex.Match(Path.GetExtension(File.FileName), "(jpe?g|png|gif)$", RegexOptions.IgnoreCase).Success);
        }

        protected string UploadImageFile(HttpPostedFileBase File)
        {
            try
            {
                if (ValidateFile(File))
                {
                    string FileName = Path.GetFileNameWithoutExtension(File.FileName);
                    string FileExt = Path.GetExtension(FileName);

                    FileName = string.Format("{0}-{1}", FileName, DateTime.Now.ToString("HH-mm-ss"));
                    FileName = string.Format("{0}.{1}", FileName, FileExt);

                    string FilePath = string.Format("~/Images/{0}", FileName);

                    FileName = Path.Combine(Server.MapPath("~/Images/"), FileName);

                    File.SaveAs(FileName);

                    return FilePath;
                }
                else throw new Exception("File Is > 3mb or Is not a 'jpg, png or jpeg' file");
            }
            catch(Exception ex) { throw ex; }
        }

        //Excel Method

    }
}