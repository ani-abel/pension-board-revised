﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BusinessLayer;
using ModelLayer.Model;

namespace PresentationLayer.Controllers
{
    public abstract class BasicSetupBaseController<T, E> : BaseController
        where T : class
        where E : class
    {
        protected BusinessBaseLogic<T, E> db;

        public BasicSetupBaseController(BusinessBaseLogic<T, E> _db)
        {
            db = _db;
        }

        protected int? Id { get; set; }
        protected Expression<Func<E, bool>> Selector { get; set; }
        protected string ModelName { get; set; }

        protected abstract bool ModifyModel(T model);

        // GET: BasicSetupBase
        public ActionResult Index()
        {
            List<T> models = null;
            try
            {
                models = db.GetAll().ToList();
            }
            catch (Exception ex) { SetMessage(string.Format("Error Occured! {0}", ex.Message), Message.Category.Error); }

            return View(models);
        }

        public virtual ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult Create(T model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    T newModel = db.Create(model);

                    if (newModel != null)
                    {
                        SetMessage(string.Format("{0} has been successfully created.", ModelName), Message.Category.Information);
                        return RedirectToAction("Index");
                    }
                }
            }
            catch (Exception ex) { SetMessage(string.Format("Error Occured! {0}", ex.Message), Message.Category.Error); }

            return View(model);
        }

        //GET: "Admin/File/Edit/5"
        public virtual ActionResult Edit(int? id)
        {
            T model = null;
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Id = id;
                model = db.GetModelBy(Selector);
                if (model == null)
                {
                    return HttpNotFound();
                }
            }
            catch (Exception ex) { SetMessage(string.Format("Error Occured! {0}", ex.Message), Message.Category.Error); }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(T model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    bool success = ModifyModel(model);
                    if (success)
                    {
                        SetMessage(string.Format("{0} modification was successfully saved.", ModelName), Message.Category.Information);
                        return RedirectToAction("Index");
                    }
                }
            }
            catch (Exception ex) { SetMessage(string.Format("Error Occured! {0}", ex.Message), Message.Category.Error); }

            return View(model);
        }

        public ActionResult Delete(int? id)
        {
            T model = null;
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Id = id;
                model = db.GetModelBy(Selector);

                if (model == null)
                {
                    return HttpNotFound();
                }
            }
            catch (Exception ex) { SetMessage(string.Format("Error Occured! {0}", ex.Message), Message.Category.Error); }
            return View(model);
        }

        // POST: /Admin/Pensioner/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            T model = null;
            try
            {
                Id = id;
                bool success = db.Delete(Selector);
                if (success)
                {
                    SetMessage(ModelName + " hass been successfully deleted.", Message.Category.Information);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex) { SetMessage(string.Format("Error Occured! {0}", ex.Message), Message.Category.Error); }
            Id = id;
            model = db.GetModelBy(Selector);
            return View(model);
        }

        public ActionResult Details(int? id)
        {
            T model = null;
            try
            {
                if (Id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Id = id;
                model = db.GetModelBy(Selector);
                if (model == null)
                {
                    return HttpNotFound();
                }
            }
            catch (Exception ex) { SetMessage(string.Format("Error Occured! {0}", ex.Message), Message.Category.Error); }

            return View(model);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }

            base.Dispose(disposing);
        }

    }
}