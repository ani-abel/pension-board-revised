﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

using BusinessLayer;
using ModelLayer.Model;
using PresentationLayer.Models;
using PresentationLayer.Controllers;

namespace PresentationLayer.Areas.Security.Controllers
{
    public class AccountController : BaseController
    {
        private UserLogic userLogic;
        private User userModel;

        public AccountController()
        {
            userLogic = new UserLogic();
            userModel = new User();
        }


        public ActionResult Home()
        {
            User user = (User)TempData.Peek("User");
            return View();
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            LoginViewModel LoginModel = new LoginViewModel();
            
            if (DoesCookieExist("Username") && DoesCookieExist("Password"))
            {
                LoginModel.Username = GetHttpCookie("Username");
                LoginModel.Password = GetHttpCookie("Password");              

                return View(LoginModel);
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult Login(LoginViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = userLogic.GetUser(model.Username, model.Password);
                    
                    if (user != null)
                    {                     
                        new PensionerPaymentsLogic().AddNewMonth();//Add a new Month To the PensionerPayments Table

                        if (model.RememberMe)
                        {
                            SetHttpCookies("Username", model.Username);
                            SetHttpCookies("Password", model.Password);
                        }

                        //Set userId as a cookie to make it easily accesed by the application at any time
                        SetHttpCookies("UserId", user.Id.ToString());
                        SetHttpCookies("UserRole", user.Role.Id.ToString());

                        if (user.Role.Id == 1)
                        {
                            FormsAuthentication.SetAuthCookie(model.Username, false);
                            return RedirectToAction("Index", "Pensioner", new { Area = "Admin" });
                        }
                        else if (user.Role.Id == 2)
                        {
                            FormsAuthentication.SetAuthCookie(model.Username, false);
                            return RedirectToAction("Index", "Home", new { Area = "" });
                        }
                    }
                    else throw new Exception("User Not Found");
                }
            }
            catch (Exception ex)
            {
                throw ex;
                SetMessage(string.Format("Error Occured! {0}", ex.Message), Message.Category.Error);
            }
            SetMessage("Invalid Username and Password", Message.Category.Error);

            return View(model);
        }
        
        [Authorize]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account", new { Area = "Security" });
        }


        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

    }
}