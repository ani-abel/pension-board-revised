﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PresentationLayer.Models;
using PresentationLayer.Controllers;
using ModelLayer.Model;
using BusinessLayer;
using System.Web.Routing;

namespace PresentationLayer.Areas.Admin.Controllers
{
    public class ManageAccountController : BaseController
    {
        private UserLogic userLogic;
        private UserRolesLogic userRolesLogic;

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);;
        }

        public ManageAccountController()
        {
            userLogic = new UserLogic();
            userRolesLogic = new UserRolesLogic();

           
        }

        // GET: Admin/ManageAccount
        [Authorize(Roles = "Admin")]
        public ActionResult Index()//For changing an user's details
        {

            try
            {
                ViewBag.Title = string.Format("Change Your Account Credentials | {0}", GetHttpCookie("Username"));

                //Patch the form values in           
                ChangePasswordVieWModel ChangeDetails = new ChangePasswordVieWModel();

                if (DoesCookieExist("Username") && DoesCookieExist("Password"))
                {
                    ChangeDetails.Username = GetHttpCookie("Username");
                    ChangeDetails.Password = GetHttpCookie("Password");
                    ChangeDetails.ConfirmPassword = GetHttpCookie("Password");
                    ChangeDetails.UserId = GetHttpCookiesAsInt("UserId");
                    ChangeDetails.UserRole = GetHttpCookiesAsInt("UserRole");
                   
                    return View(ChangeDetails);
                }

            }
            catch(Exception ex) { throw ex; }

            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult Index(ChangePasswordVieWModel model)
        {
            try
            {
                //Patch the userId saved as a cookie into the model
                model.UserId = Int32.Parse(GetHttpCookie("UserId"));

                if (ModelState.IsValid)
                {
                    var translatedRoles = userRolesLogic.GetModelBy(ur => ur.id == model.UserRole);
                    var similarUser = userLogic.CountEntityForCondition(u => u.username == model.Username && u.password == model.Password);

                    if (similarUser > 0)
                    {
                        throw new Exception("These Credentials already exist");
                    }
                    //update the db with these details
                    userLogic.Update(new User()
                    {
                        Id = model.UserId,//Reset this field after sorting out issue with Login-in
                        Username = model.Username,
                        Password = model.ConfirmPassword,
                        Role = translatedRoles
                    });

                    //Reset the cookies found in the browser
                    SetHttpCookies("Username", model.Username);
                    SetHttpCookies("Password", model.Password);

                    return RedirectToAction("Index", "Pensioner", new { Area = "Admin" });
                }
            }
            catch(Exception ex)
            {
                SetMessage(string.Format("Error Occured! {0}", ex.Message), Message.Category.Error);
                throw ex;
            }
            return View(model);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult AddNewAdmin()//For registering a new user
        {
            try
            {
                ViewBag.Title = string.Format("Add New Admin | {0}", GetHttpCookie("Username"));
                PopulateUserRolesField();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult AddNewAdmin(RegisterViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var translatedRole = userRolesLogic.GetModelBy(ur => ur.id == model.Role);
                    //Get a single userModel from the DB
                    var similarModel = userLogic.CountEntityForCondition(u => u.username == model.Username && u.password == model.Password);

                    if(similarModel > 0)
                    {
                        throw new Exception("These Credentials already exist");                      
                    }

                    userLogic.Create(new User()
                    {
                        Username = model.Username,
                        Password = model.ConfirmPassword,
                        Role = translatedRole
                    });

                    return RedirectToAction("Index", "Pensioner", new { area = "Admin" });
                }
            }
            catch(Exception ex)
            {
                SetMessage(string.Format("Error Occured! {0}", ex.Message), Message.Category.Error);
                throw ex;
            }
            return View(model);
        }

        private void PopulateUserRolesField()
        {
            var userRoles = userRolesLogic.GetKeyValuePairs();
            if (userRoles != null && userRoles.Count > 0)
            {
                ViewBag.UserRoleId = userRoles;
            }
        }
    }
}