﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PresentationLayer.Models;
using ModelLayer;
using BusinessLayer;
using PresentationLayer.Controllers;
using PresentationLayer.Areas.Admin.ViewModels;

namespace PresentationLayer.Areas.Admin.Controllers
{
    public class PensionerController : BaseController
    {
        // GET: Admin/Pensioner
        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            ViewBag.Title = string.Format("Welcome {0} | View All Pensioners", DoesCookieExist("Username") ? GetHttpCookie("Username") : "Admin");
            return View();
        }

        [Authorize(Roles = "Admin")]
        public ActionResult ViewPensioners()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        public ActionResult AddPensioner()
        {
            ViewBag.Title = string.Format("Add Pensioner | {0}", DoesCookieExist("Username") ? GetHttpCookie("Username") : "Admin");
            return View();
        }
        //Fill Select Options
        //BaseController: Upload File
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult AddPensioner(AddPensionerViewModel model)
        {
            try
            {              
                throw new Exception("Model Seems to work");
            }
            catch(Exception ex) { throw ex; }

            return View();
        }

        public ActionResult EmploymentData()
        {
            return View();
        }

        public ActionResult BankData()
        {
            return View();
        }

        public ActionResult OriginData()
        {
            return View();
        }

        public ActionResult PersonalData()
        {
            return View();
        }

        public ActionResult NextOfKinData()
        {
            return View();
        }

        public ActionResult EditPersonalData()
        {
            return View();
        }

        public ActionResult EditOriginData()
        {
            return View();
        }

        public ActionResult EditEmploymentData()
        {
            return View();
        }

        public ActionResult EditBankData()
        {
            return View();
        }

        public ActionResult EditNextOfKinData()
        {
            return View();
        }

        public ActionResult AllDetails()
        {
            return View();
        }

    }
}