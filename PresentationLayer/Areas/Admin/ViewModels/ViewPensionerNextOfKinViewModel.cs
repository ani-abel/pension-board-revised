﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PresentationLayer.Areas.Admin.ViewModels
{
    public class ViewPensionerNextOfKinViewModel
    {
        public string PensionerName { get; set; }
        public string Passport { get; set; }
        public string FullName { get; set; }//Added
        public string PhoneNo { get; set; }
        public string Relationship { get; set; }//fk
        public string Gender { get; set; }//fk
        public string PermanentAddress { get; set; }
    }
}