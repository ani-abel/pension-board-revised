﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PresentationLayer.Areas.Admin.ViewModels
{
    public class ViewPensionerBankDataViewModel
    {
        public string PensionerName { get; set; }
        public string Bank { get; set; }
        public string BankBranch { get; set; }
        public string AccountNumber { get; set; }
        public long GrossPension { get; set; }
        public long NetPension { get; set; }
        public long Deductible { get; set; }
    }
}