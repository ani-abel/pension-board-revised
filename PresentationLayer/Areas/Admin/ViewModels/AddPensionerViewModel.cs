﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PresentationLayer.Areas.Admin.ViewModels
{
    public class AddPensionerViewModel
    {
        //Data Like "Marital Status", "Gender", "Title" 
        public EditPersonalDetailsViewModel Pensioner { get; set; }

        //Origin Data Like "State", "Lga"
        public EditOriginDetailsViewModel OriginDetails { get; set; }

        //Employent Data Like "Last Employment"
        public EditEmploymentDetailsViewModel EmploymentDetails { get; set; }

        //Next Of Kin Details Like "Surname, OtherNames, Passport"
        public EditNextOfKinDetailsViewModel NextOfKinDetails { get; set; }

        //Bank Details Like: "Bank, BankBranch"
        public EditBankDetailsViewModel BankDetails { get; set; }
        
    }
}