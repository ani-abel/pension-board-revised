﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PresentationLayer.Areas.Admin.ViewModels
{
    public class EditOriginDetailsViewModel
    {
        public int PensionerId { set; get; }
        public string Nationality { get; set; }
        public string State { get; set; }
        public string Lga { get; set; }

        [DisplayName("Home Town")]
        public string HomeTown { set; get; }

        [DisplayName("Permanent Home Address")]
        public string PermanentHomeAddress { get; set; }

        [DisplayName("Date Of Birth")]
        [DataType(DataType.DateTime)]
        public DateTime Dob { get; set; }

        [DisplayName("Phone Number")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNo { get; set; }
    }
}