﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PresentationLayer.Areas.Admin.ViewModels
{
    public class ViewPensionerEmploymentDataViewModel
    {
        public string PensionerName { get; set; }
        public DateTime DateOfFirstAppointment { get; set; }
        public DateTime DateOfLastAppointment { get; set; }
        public string LastPost { get; set; }
        public string GradeAsAtRetirement { get; set; }
        public int YearsInService { get; set; }
        public string ReasonforRetirement { set; get; }
    }
}