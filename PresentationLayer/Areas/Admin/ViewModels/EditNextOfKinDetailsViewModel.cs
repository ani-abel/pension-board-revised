﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace PresentationLayer.Areas.Admin.ViewModels
{
    public class EditNextOfKinDetailsViewModel
    {
        public int NOkId { get; set; }
        public int PensionerId { get; set; }
        public string Surname { get; set; }//Added

        [DisplayName("Other Names")]
        public string OtherNames { get; set; }

        [DisplayName("Phone Number")]
        public string PhoneNo { get; set; }
        public int Relationship { get; set; }
        public int Gender { get; set; }

        [DisplayName("Permenent House Address")]
        public string PermenentHouseAddress { get; set; }

        public string ImageUrl { get; set; }

        [DisplayName("Image File")]
        public HttpPostedFile ImageFile { get; set; }
    }
}