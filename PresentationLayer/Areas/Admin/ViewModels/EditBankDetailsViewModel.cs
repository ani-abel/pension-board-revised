﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PresentationLayer.Areas.Admin.ViewModels
{
    public class EditBankDetailsViewModel
    {
        public int PensionerId { get; set; }
        public int Bank { get; set; }

        [DisplayName("Bank Branch")]
        public string BankBranch { get; set; }

        [DisplayName("Account Number")]
        public string AccountNo { get; set; }

        [DisplayName("Gross Pension")]
        [DataType(DataType.Currency)]
        public long GrossPension { get; set; }

        [DataType(DataType.Currency)]
        public long Deductible { get; set; }
    }
}