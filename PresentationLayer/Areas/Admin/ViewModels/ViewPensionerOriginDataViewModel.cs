﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PresentationLayer.Areas.Admin.ViewModels
{
    public class ViewPensionerOriginDataViewModel
    {
        public string PensionerName { get; set; }
        public string Nationality { get; set; }
        public string Lga { get; set; }
        public string HomeTown { get; set; }
        public string PermanentHomeAddress { get; set; }
        public DateTime Dob { get; set; }
        public string PhoneNo { get; set; }
    }
}