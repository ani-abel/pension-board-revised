﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PresentationLayer.Areas.Admin.ViewModels
{
    public class ViewPensionerPersonalDataViewModel
    {
        public string ImageUrl { get; set; }
        public string Surname { get; set; }
        public string OtherNames { get; set; }
        public string Gender { get; set; }
        public string MaritalStatus { get; set; }
        public DateTime DateCreated { get; set; }
        public string SpouseName { get; set; }

    }
}