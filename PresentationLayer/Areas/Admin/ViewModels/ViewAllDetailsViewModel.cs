﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PresentationLayer.Areas.Admin.ViewModels
{
    public class ViewAllDetailsViewModel
    {
        public ViewPensionerPersonalDataViewModel PersonalData { get; set; }
        public ViewPensionerOriginDataViewModel OriginData { get; set; }
        public ViewPensionerEmploymentDataViewModel EmploymentData { set; get; }
        public ViewPensionerNextOfKinViewModel NextOfKinData { get; set; }
        public ViewPensionerBankDataViewModel BankData { get; set; }
    }
}