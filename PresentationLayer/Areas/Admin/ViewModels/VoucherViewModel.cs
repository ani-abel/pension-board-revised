﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ModelLayer.Model;

namespace PresentationLayer.Areas.Admin.ViewModels
{
    public class VoucherViewModel
    {
        public string PensionerName { get; set; }
        public DateTime IssuedOn { get; set; }
        public string LastPost { get; set; }
        public  string GradeAtRetirement { get; set; }
        public string IssuedBy { get; set; }
    }
}