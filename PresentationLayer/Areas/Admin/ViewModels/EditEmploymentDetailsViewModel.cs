﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PresentationLayer.Areas.Admin.ViewModels
{
    public class EditEmploymentDetailsViewModel
    {
        public int PensionerId { get; set; }//Use ViewBag

        public string PensionerName { get; set; }//Get Detail from "PENSIONER"

        [DisplayName("First Appointment")]
        [DataType(DataType.DateTime)]
        public DateTime DateOfFirstAppointment { get; set; }

        [DisplayName("Last Appointment")]
        [DataType(DataType.DateTime)]
        public DateTime DateOfLastAppointment { get; set; }

        [DisplayName("Last Post")]
        public string LastPost { get; set; }

        [DisplayName("Grade At Retirement")]
        public string GradeAsAtRetirement { get; set; }

        [DisplayName("Years In Service")]
        public int YearsInService { get; set; }

        [DisplayName("Reason For Retirement")]
        public int ReasonforRetirement { set; get; }
    }
}