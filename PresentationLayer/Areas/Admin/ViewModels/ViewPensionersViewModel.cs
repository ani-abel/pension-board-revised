﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PresentationLayer.Areas.Admin.ViewModels
{
    public class ViewPensionersViewModel
    {
        public int PensionerId { get; set; }
        public string FullName { get; set; }
        public string LastPost { get; set; }
        public string GradeAtRetirement { get; set; }
        public string NextOfKinName { get; set; }
        //Edit button avaliable
    }
}