﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PresentationLayer.Areas.Admin.ViewModels
{
    public class EditPersonalDetailsViewModel
    {
        public int Title { get; set; }
        public string Surname { get; set; }

        [Display(Name = "Other Names")]
        public string OtherNames { get; set; }
        public int Gender { get; set; }

        [Display(Name = "Marital Status")]
        public int MaritalStatus { get; set; }

        [Display(Name = "Spouse Name")]
        public string SpouseName { get; set; }

        public string ImageUrl { get; set; }

        [Display(Name = "Upload Image")]
        public HttpPostedFile ImageFile { get; set; }

    }
}