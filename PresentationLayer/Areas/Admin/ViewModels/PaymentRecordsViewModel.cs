﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PresentationLayer.Areas.Admin.ViewModels
{
    public class PaymentRecordsViewModel
    {
        public int Id { set; get; }
        public DateTime PaymentMadeOn { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
    }
}