﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Translator;
using ModelLayer.Entity;
using ModelLayer.Model;

namespace BusinessLayer
{
    public class UserRolesLogic : BusinessBaseLogic<UserRoles, USER_ROLES>
    {
        public UserRolesLogic()
        {
            translator = new UserRolesTranslator();
        }

        public List<Value> GetKeyValuePairs()
        {//Used to populate <Select> fields
            try
            {
                List<Value> values = new List<Value>();
                values.Add(new Value() {  Id = 0, Name = "------ Select Role ------" });
                var dbItems = GetAll();

                foreach (var item in dbItems)
                {
                    values.Add(new Value() { Id = item.Id, Name = item.Role });
                }
                return values;
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
