﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Entity;
using ModelLayer.Model;
using ModelLayer.Translator;

namespace BusinessLayer
{
    public class UserLogic : BusinessBaseLogic<User, USER>
    {
        public UserLogic()
        {
            translator = new UserTranslator();
        }

        public bool ValidateUser(string username, string password)
        {
            try
            {
                return (GetUser(username, password) != null);
            }
            catch (Exception ex) { throw ex; }
        }

        public User GetUser(string username, string password)
        {
            User User = GetModelsBy(u => u.username == username && u.password == password).FirstOrDefault();
            if (User != null)
            {
                return new User()
                {
                    Id = User.Id,
                    Username = User.Username,
                    Password = User.Password,
                    Role = User.Role
                };
            }
            throw new Exception(NoUserFound);
        }

        public User HardCodeAdminDetails()
        {
            return new User()
            {
                Id = 1,
                Username = "admin@pencom.com",
                Password = "secret"
            };
        }

        public User HardCodeUserDetails()
        {
            return new User()
            {
                Id = 2,
                Username = "user@pencom.com",
                Password = "secretuser"
            };
        }
        public List<Value> GetKeyValuePairs()
        {//Used to populate <Select> fields
            try
            {
                List<Value> values = new List<Value>();
                foreach (var item in GetAll())
                {
                    values.Add(new Value() { Id = item.Id, Name = item.Role.Role });
                }
                return values;
            }
            catch (Exception ex) { throw ex; }
        }

        //private bool DuplicateUserDetails(string username, string password)
        //{
        //    List<User> User = GetModelsBy(u => u.username == username && u.password == password);
        //    if(User != null)
        //    {
        //        return (User.Count > 1 ? true : false);
        //    }
        //    throw new Exception(NoUserFound);
        //}

        //private bool DeleteDuplicateUser(string username, string password)
        //{
        //    User user = GetModelBy(u => u.username == username && u.password == password);
        //    return Delete(user.Id);
        //}
    }
}
