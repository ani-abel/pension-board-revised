﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Translator;
using ModelLayer.Model;
using ModelLayer.Entity;

namespace BusinessLayer
{
    public class GenderLogic : BusinessBaseLogic<Gender, GENDER>
    {
        public GenderLogic()
        {
            translator = new GenderTranslator();
        }

        public List<Value> GetAllGenders()
        {
            List<Value> values = new List<Value>();
            List<Gender> models = GetAll();

            values.Add(new Value() { Id = 0, Name = "----- Select Gender -----" });
            foreach (var item in models)
            {
                values.Add(new Value() { Id = item.Id, Name = item.GenderType });
            }

            return values;
        }

    }
}