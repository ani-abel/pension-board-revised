﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Entity;
using ModelLayer.Model;
using ModelLayer.Translator;

namespace BusinessLayer
{
    public class TitleLogic : BusinessBaseLogic<Titles, TITLE>
    {
        public TitleLogic()
        {
            translator = new TitleTranslator();
        }

        public List<Value> GetAllTitles()
        {
            List<Value> values = new List<Value>();
            List<Titles> models = GetAll();

            values.Add(new Value() { Id = 0, Name = "----- Select Title -----" });
            foreach (var item in models)
            {
                values.Add(new Value() { Id = item.Id, Name = item.Title });
            }

            return values;
        }
    }
}
