﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Entity;
using ModelLayer.Model;
using ModelLayer.Translator;

namespace BusinessLayer
{
    public class JobPostsLogic : BusinessBaseLogic<JobPosts, JOB_POSTS>
    {
        public JobPostsLogic()
        {
            translator = new JobPostsTranslator();
        }

        public List<Value> GetAllJobPosts()
        {
            List<Value> values = new List<Value>();
            List<JobPosts> models = GetAll();

            values.Add(new Value() { Id = 0, Name = "----- Select Job Posts -----" });
            foreach (var item in models)
            {
                values.Add(new Value() { Id = item.Id, Name = item.JobPosting });
            }

            return values;
        }
    }
}
