﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Entity;
using ModelLayer.Model;
using ModelLayer.Translator;

namespace BusinessLayer
{
    public class PensionerPersonalDataLogic : BusinessBaseLogic<PensionerPersonalData, PENSIONER_PERSONAL_DATA>
    {
        public PensionerPersonalDataLogic()
        {
            translator = new PensionerPersonalDataTranslator();
        }
    }
}
