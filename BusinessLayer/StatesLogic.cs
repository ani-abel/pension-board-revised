﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Entity;
using ModelLayer.Model;
using ModelLayer.Translator;

namespace BusinessLayer
{
    public class StatesLogic : BusinessBaseLogic<States, STATE>
    {
        public StatesLogic()
        {
            translator = new StatesTranslator();
        }

        public List<Value> GetAllStates()
        {
            List<Value> values = new List<Value>();
            List<States> models = GetAll();

            values.Add(new Value() { Id = 0, Name = "----- Select State -----"});
            foreach (var item in models)
            {
                values.Add(new Value() { Id = item.Id, Name = item.State });
            }

            return values;
        }
    }
}
