﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Model;
using ModelLayer.Translator;
using DataLayer;
using ModelLayer.Entity;
using System.Transactions;

namespace BusinessLayer
{
    public class DbTransaction
    {
        private IRepository repository = new Repository();

        private PensionerLogic PensionerLogic;//Data Like "Marital Status", "Gender", "Title" 
        private PensionerTranslator PensionerTranslator;

        private PensionerPersonalDataLogic PensionerPersonalDataLogic;//Origin Data
        private PensionerPersonalDataTranslator PensionerPersonalDataTranslator;

        private PensionerEmploymentDataLogic PensionerEmploymentDataLogic;//Employment Data
        private PensionerEmploymentDataTranslator PensionerEmploymentDataTranslator;

        private PensionerNextOfKinLogic PensionerNextOfKinLogic;//Next Of Kin Data
        private PensionerNextOfKinDataTranslator PensionerNextOfKinTranslator;

        private PensionerPaymentDataLogic PensionerPaymentDataLogic;//Bank Details
        private PensionerPaymentDataTranslator PensionerPaymentDataTranslator;

        public DbTransaction()
        {
            PensionerLogic = new PensionerLogic();
            PensionerTranslator = new PensionerTranslator();

            PensionerPersonalDataLogic = new PensionerPersonalDataLogic();
            PensionerPersonalDataTranslator = new PensionerPersonalDataTranslator();

            PensionerEmploymentDataLogic = new PensionerEmploymentDataLogic();
            PensionerEmploymentDataTranslator = new PensionerEmploymentDataTranslator();

            PensionerNextOfKinLogic = new PensionerNextOfKinLogic();
            PensionerNextOfKinTranslator = new PensionerNextOfKinDataTranslator();

            PensionerPaymentDataLogic = new PensionerPaymentDataLogic();
            PensionerPaymentDataTranslator = new PensionerPaymentDataTranslator();
        }

        public bool SaveAllModels(Pensioner Pensioner, PensionerPersonalData PersonalData, 
            PensionerEmploymentData EmploymentData, PensionerNextOfKinData NextOfKinData,
            PensionerPaymentData PaymentData)
        {          
            using (TransactionScope transaction = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Snapshot }))
            {
                bool transactionIsComplete = false;

                try
                {
                    //Translate Models To Entities
                    PENSIONER PensionerEntity = PensionerTranslator.TranslateToEntity(Pensioner);

                    PENSIONER_PERSONAL_DATA PersonalDataEntity = PensionerPersonalDataTranslator.TranslateToEntity(PersonalData);

                    PENSIONER_EMPLOYMENT_DATA EmploymentDataEntity = PensionerEmploymentDataTranslator.TranslateToEntity(EmploymentData);

                    PENSIONER_NEXT_OF_KIN_DATA NextOfKinEntity = PensionerNextOfKinTranslator.TranslateToEntity(NextOfKinData);

                    PENSIONER_PAYMENT_DATA PaymentDataEntity = PensionerPaymentDataTranslator.TranslateToEntity(PaymentData);
                    
                    repository.Add(PensionerEntity);
                    repository.Save();

                    repository.Add(PersonalDataEntity);
                    repository.Save();

                    repository.Add(EmploymentDataEntity);
                    repository.Save();

                    repository.Add(NextOfKinEntity);
                    repository.Save();

                    repository.Add(PaymentDataEntity);
                    repository.Save();

                    //Complete the DB Transaction
                    transaction.Complete();

                    transactionIsComplete = true;
                }
                catch (Exception ex)
                {
                    transaction.Dispose();
                    throw ex;
                }

                return transactionIsComplete;
            }         
        }

        //Find A Way To Add Without Committing To DB

        //public ActionResult Index(User ObjUserData)
        //{
        //    using (var context = new MultiFileDbContext())
        //    {
        //        using (DbContextTransaction dbTran = context.Database.BeginTransaction())
        //        {
        //            try
        //            {
        //                User pd = new User()
        //                {
        //                    EmplCode = ObjUserData.EmplCode,
        //                    EmplName = ObjUserData.EmplName
        //                };
        //                context.ObjUserData.Add(pd);
        //                context.SaveChanges();

        //                var id = pd.RefUserID;

        //                var usrFile = ObjUserData.objUsrEducation;
        //                if (usrFile != null)
        //                {
        //                    foreach (var item in usrFile)
        //                    {
        //                        item.RefUserID = id;
        //                        context.objUserEducation.Add(item);
        //                    }
        //                }
        //                context.SaveChanges();
        //                dbTran.Commit();
        //            }
        //            catch (DbEntityValidationException ex)
        //            {
        //                dbTran.Rollback();
        //                throw;
        //            }
        //        }
        //    }
        //    return View();
        //}

    }
}
