﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Entity;
using ModelLayer.Model;
using ModelLayer.Translator;

namespace BusinessLayer
{
    public class PensionerDeathCertificateLogic : BusinessBaseLogic<PensionerDeathCertificate, PENSIONER_DEATH_CERTIFICATE>
    {
        public PensionerDeathCertificateLogic()
        {
            translator = new PensionerDeathCertificateTranslator();
        }
    }
}
