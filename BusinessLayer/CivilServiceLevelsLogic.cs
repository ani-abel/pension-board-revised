﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Entity;
using ModelLayer.Model;
using ModelLayer.Translator;

namespace BusinessLayer
{
    public class CivilServiceLevelsLogic : BusinessBaseLogic<CivilServiceLevel, CIVIL_SERVICE_LEVEL>
    {
        public CivilServiceLevelsLogic()
        {
            translator = new CivilServiceLevelTranslator();
        }

        public List<Value> GetAllCivilServiceLevels()
        {
            List<Value> values = new List<Value>();
            List<CivilServiceLevel> models = GetAll();

            values.Add(new Value() { Id = 0, Name = "----- Select Civil Service Level -----" });
            foreach (var item in models)
            {
                values.Add(new Value() { Id = item.Id, Name = item.ServiceLevel });
            }

            return values;
        }
    }
}
