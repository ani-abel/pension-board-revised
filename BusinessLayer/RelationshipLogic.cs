﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Entity;
using ModelLayer.Model;
using ModelLayer.Translator;

namespace BusinessLayer
{
    public class RelationshipLogic : BusinessBaseLogic<Relationships, RELATIONSHIP>
    {
        public RelationshipLogic()
        {
            translator = new RelationshipTranslator();
        }

        public List<Value> GetAllRelationships()
        {
            List<Value> values = new List<Value>();
            List<Relationships> models = GetAll();

            values.Add(new Value() { Id = 0, Name = "----- Select Relationship -----" });
            foreach (var item in models)
            {
                values.Add(new Value() { Id = item.Id, Name = item.Relationship });
            }

            return values;
        }
    }
}
