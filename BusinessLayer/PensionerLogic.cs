﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Entity;
using ModelLayer.Model;
using ModelLayer.Translator;

namespace BusinessLayer
{
    public class PensionerLogic : BusinessBaseLogic<Pensioner, PENSIONER>
    {

        public PensionerLogic()
        {
            translator = new PensionerTranslator();
        }

        public override List<Pensioner> GetAll()
        {
            return base.GetAll();
        }

        public Pensioner GetBy(long Id)
        {
            try
            {
                Expression<Func<PENSIONER, bool>> selector = p => p.id == Id;
                return base.GetModelBy(selector);
            }
            catch (Exception ex) { throw ex; }
        }

        public override Pensioner Create(Pensioner model)
        {
            return base.Create(model);
        }//Adds a new user and then returns the user details as an object

        public bool Modify(Pensioner pensioner)
        {
            try
            {
                PENSIONER entity = GetEntityBy(p => p.id == pensioner.Id);
                entity.id = pensioner.Id;
                if (pensioner.Title != null)
                {
                    entity.title = pensioner.Title.Id;
                }
                if (pensioner.Surname != null)
                {
                    entity.surname = pensioner.Surname;
                }
                if (pensioner.OtherNames != null)
                {
                    entity.other_names = pensioner.OtherNames;
                }
                if (pensioner.Gender != null && (pensioner.Gender.Id == 1 || pensioner.Gender.Id == 2))
                {
                    entity.gender = pensioner.Gender.Id;
                }
                if (pensioner.MaritalStatus != null)
                {
                    entity.marital_status = pensioner.MaritalStatus.Id;
                }
                if (pensioner.MaidianName != null && canHaveMaidianName(pensioner.Gender.Id, pensioner.MaritalStatus.Id))
                {
                    entity.maidian_name = pensioner.MaidianName;
                }
                if (pensioner.SpouseName != null)
                {
                    entity.spouse_name = pensioner.SpouseName;
                }
                if (pensioner.IsDeceased.ToString() != null)
                {
                    entity.deceased = pensioner.IsDeceased;
                }

                int modifiedRecordCount = Save();

                if (modifiedRecordCount <= 0)
                {
                    throw new Exception(NoItemModified);
                }
                return true;
            }
            catch (Exception ex) { throw ex; }
        }

        private bool canHaveMaidianName(int genderId, int maritalStatusId)
        {
            return !(genderId == 1 && maritalStatusId == 2);//gender.Id(1) = Male && maritalStatus.Id(2) = Single
        }

    }
}
