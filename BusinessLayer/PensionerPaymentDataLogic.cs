﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Entity;
using ModelLayer.Model;
using ModelLayer.Translator;

namespace BusinessLayer
{
    public class PensionerPaymentDataLogic : BusinessBaseLogic<PensionerPaymentData, PENSIONER_PAYMENT_DATA>
    {
        public PensionerPaymentDataLogic()
        {
            translator = new PensionerPaymentDataTranslator();
        }
    }
}
