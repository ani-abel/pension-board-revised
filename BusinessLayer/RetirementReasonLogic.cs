﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Entity;
using ModelLayer.Model;
using ModelLayer.Translator;

namespace BusinessLayer
{
    public class RetirementReasonLogic : BusinessBaseLogic<RetirementReason, RETIREMENT_REASON>
    {
        public RetirementReasonLogic()
        {
            translator = new RetirementReasonTranslator();
        }

        public List<Value> GetAllReasonsForRetirement()
        {
            List<Value> values = new List<Value>();
            List<RetirementReason> models = GetAll();

            values.Add(new Value() { Id = 0, Name = "----- Select Retirement Reason -----" });
            foreach (var item in models)
            {
                values.Add(new Value() { Id = item.Id, Name = item.Reason });
            }

            return values;
        }
    }
}
