﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Entity;
using ModelLayer.Model;
using ModelLayer.Translator;

namespace BusinessLayer
{
    public class PensionerNextOfKinLogic : BusinessBaseLogic<PensionerNextOfKinData, PENSIONER_NEXT_OF_KIN_DATA>
    {
        public PensionerNextOfKinLogic()
        {
            translator = new PensionerNextOfKinDataTranslator();
        }
    }
}
