﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Entity;
using ModelLayer.Model;
using ModelLayer.Translator;

namespace BusinessLayer
{
    public class PensionerEmploymentDataLogic : BusinessBaseLogic<PensionerEmploymentData, PENSIONER_EMPLOYMENT_DATA>
    {
        public PensionerEmploymentDataLogic()
        {
            translator = new PensionerEmploymentDataTranslator();
        }
    }
}
