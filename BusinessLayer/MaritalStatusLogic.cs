﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Entity;
using ModelLayer.Model;
using ModelLayer.Translator;

namespace BusinessLayer
{
    public class MaritalStatusLogic : BusinessBaseLogic<MaritalStatus, MARITAL_STATUS>
    {
        public MaritalStatusLogic()
        {
            translator = new MaritalStatusTranslator();
        }

        public List<Value> GetAllMaritalStatuses()
        {
            List<Value> values = new List<Value>();
            List<MaritalStatus> models = GetAll();

            values.Add(new Value() { Id = 0, Name = "----- Select Marital Status -----" });
            foreach (var item in models)
            {
                values.Add(new Value() { Id = item.Id, Name = item.Status });
            }

            return values;
        }
    }
}
