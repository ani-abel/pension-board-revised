﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Entity;
using ModelLayer.Model;
using ModelLayer.Translator;

namespace BusinessLayer
{
    public class PensionerPaymentsLogic : BusinessBaseLogic<PensionerPayments, PENSIONERS_PAYMENTS>
    {
        public PensionerPaymentsLogic()
        {
            translator = new PensionerPaymentsTranslator();
        }

        public List<PensionerPayments> GetPaidMonths()
        {
            return base.GetAll();
        }

        public List<String> GetPaidMonthsWithOutId()
        {
            List<string> Months = new List<string>();

            foreach (var item in GetPaidMonths())
            {
                Months.Add(string.Format("{0} {1}", item.Month, item.Year));
            }
            return Months;
        }
        //Main Issues: 1. Session Not working, 2. Program Not Pulling data from DB

        public void AddNewMonth()
        {
            DateTime now = DateTime.Now;
            string Month = now.AddMonths(1).ToString("MMMM");
            string Year = (now.Month == 12 ? now.AddYears(1).ToString("yyyy") : now.ToString("yyyy"));

            if (now.Day >= 25 && now.AddMonths(1).Day <= 7)//From  the 25th of Every month - 7th of the next make an entry
            {
                if (CheckIfMonthExists(Month, Year))
                {
                    Create(new PensionerPayments() {
                        Month = Month,
                        Year = Year,
                        PaymentIssued = 0//Means payment Not Yet made
                    });
                }
            }
        }

        private bool CheckIfMonthExists(string Month, string Year)
        {
            var dbEntry = GetModelBy(m => m.month == Month && m.year == Year);
            return (dbEntry != null);
        }

        public List<Value> GetUnPaidMonths(string StartMonth, string criteria = "UnPaid")//Options Paid && Unpaid
        {
            List<Value> UnPaidMonths = new List<Value>();
            var list = GetModelsBy(m => m.paymentIssued == (criteria == "UnPaid" ? 0 : 1));

            if(list != null && list.Count() > 0)
            {
                foreach (var item in list)
                {
                    UnPaidMonths.Add(new Value() {  Id = item.Id, Name = string.Format("{0} {1}", item.Month, item.Year) });
                }
            }
            return UnPaidMonths;
        }

    }
}