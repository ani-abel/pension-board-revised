﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Entity;
using ModelLayer.Model;
using ModelLayer.Translator;

namespace BusinessLayer
{
    public class BankLogic : BusinessBaseLogic<Bank, BANK>
    {
        public BankLogic()
        {
            //translator object comes from "BusinessBaseLogic" class
            translator = new BankTranslator();
        }

        public List<Value> GetAllBanks()
        {
            List<Value> values = new List<Value>();
            List<Bank> models = GetAll();

            values.Add(new Value() { Id = 0, Name = "----- Select Bank -----" });
            foreach (var item in models)
            {
                values.Add(new Value() { Id = item.Id, Name = item.BankName });
            }

            return values;
        }
    }
}
