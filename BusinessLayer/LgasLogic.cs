﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Model;
using ModelLayer.Entity;
using ModelLayer.Translator;

namespace BusinessLayer
{
    public class LgasLogic: BusinessBaseLogic<Lgas, LGA>
    {
        public LgasLogic()
        {
            translator = new LgasTranslator();
        }

        public List<Value> GetAllLgas()
        {
            List<Value> values = new List<Value>();
            List<Lgas> models = GetAll();

            values.Add(new Value() { Id = 0, Name = "----- Select Local Govt. Area -----" });
            foreach (var item in models)
            {
                values.Add(new Value() { Id = item.Id, Name = item.Lga });
            }

            return values;
        }
    }
}
