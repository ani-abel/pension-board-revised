//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ModelLayer.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class PENSIONERS_PAYMENTS
    {
        public int id { get; set; }
        public Nullable<System.DateTime> payment_made_on { get; set; }
        public string month { get; set; }
        public string year { get; set; }
        public Nullable<int> paymentIssued { get; set; }
    }
}
