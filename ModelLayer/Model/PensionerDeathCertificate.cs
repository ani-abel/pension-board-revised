﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Model
{
    public class PensionerDeathCertificate
    {
        public int Id { get; set; }

        public Pensioner Pensioner { get; set; }//Represents "Pensioner_id" in the Entity class

        public int IsValidated { get; set; }//Just a normal integer; not matching a fk and used as a boolean

        public DateTime DateOfDeath { get; set; }

        public string DeathCertificate { get; set; }//Represents the path to the "Scanned" death certficate

        public DateTime DateValidated { get; set; }

        public PensionerDeathCertificate()
        {
            if(DateValidated == null)
            {
                //Set default value for this dateValidated
                DateValidated = DateTime.Now;
            }
        }
    }
}
