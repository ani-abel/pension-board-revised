﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Model
{
    public class RetirementReason
    {
        public int Id { get; set; }

        public string Reason { get; set; }
    }
}
