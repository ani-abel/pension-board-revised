﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Model
{
    public class PensionerPayments
    {
        public int Id { get; set; }
        public DateTime PaymentMadeOn { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public int PaymentIssued { get; set; }
    }
}
