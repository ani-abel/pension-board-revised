﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Model
{
    public class PensionerPersonalData
    {
        public int Id { get; set; }
        public string PhoneNo { get; set; }
        public States State { get; set; }
        public string Hometown { get; set; }
        public string Lga { get; set; }
        public string Nationality { get; set; }
        public string PermanentHomeAddress { get; set; }
        public DateTime DateOfBirth { get; set; }
        public Pensioner Pensioner { get; set; }
    }
}
