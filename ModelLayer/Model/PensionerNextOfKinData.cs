﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Model
{
    public class PensionerNextOfKinData
    {
        public int Id { get; set; }
        public string passport { get; set; }
        public string Phoneno { get; set; }
        public Relationships Relationship { get; set; }
        public Gender Gender { get; set; }
        public string PermanentAddress { get; set; }
        public int IsReceivingPension { get; set; }//Defaults to 0 => false
        public string Surname { get; set; }
        public string OtherNames { get; set; }
        public Pensioner Pensioner { get; set; }
    }
}
