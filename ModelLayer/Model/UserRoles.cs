﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Model
{
    public class UserRoles
    {
        public int Id { set; get; }
        public string Role { set; get; }
    }
}
