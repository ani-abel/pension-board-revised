﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Model
{
    public class Pensioner
    {
        public int Id { get; set; }

        public Titles Title { get; set; }

        public string Surname { get; set; }
        public string OtherNames { get; set; }

        public Gender Gender { get; set; }

        public MaritalStatus MaritalStatus { get; set; }

        public DateTime DateCreated { get; set; }

        public string SpouseName { get; set; }
        public string MaidianName { get; set; }

        //"Deceased" field is actually a boolean field
        //Do not mistake for a foreign-key field
        public int IsDeceased { get; set; }

        public string Passport { get; set; }//Path to the saved image of Pensioner
    }
}
