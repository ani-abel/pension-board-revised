﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Model
{
    public class PensionerEmploymentData
    {
        public int Id { get; set; }

        public DateTime DateOfFirstEmployment { get; set; }
        public DateTime DateOfLastEmplyment { get; set; }
        public JobPosts LastPost { get; set; }
        public CivilServiceLevel GradeAsAtRetirement { get; set; }
        public RetirementReason RetirementReason { get; set; }
        public int LenghtOfSeason { get; set; }
        public Pensioner Pensioner { get; set; }
    }
}
