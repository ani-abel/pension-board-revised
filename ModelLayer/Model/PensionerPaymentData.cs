﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Model
{
    public class PensionerPaymentData
    {
        public int Id { get; set; }
        public Bank Bank { get; set; }
        public string BankBranch { get; set; }
        public string AccountNo { get; set; }
        public long GrossPension { get; set; }
        public long Deductible { get; set; }
        public long NetPension { get; set; }
        public Pensioner Pensioner { get; set; }
    }
}
