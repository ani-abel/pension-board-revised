﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Model
{
    public class Titles
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}
