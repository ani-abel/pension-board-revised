﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModelLayer.Model
{
    public class Lgas
    {
        public int Id { get; set; }
        public string Lga { get; set; }
        public States State { get; set; }
    }
}
