﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Entity;
using ModelLayer.Model;

namespace ModelLayer.Translator
{
    public class RelationshipTranslator: TranslatorBase<Relationships, RELATIONSHIP>
    {
        public override RELATIONSHIP TranslateToEntity(Relationships model)
        {
            try
            {
                RELATIONSHIP entity = null;
                if(model != null)
                {
                    entity = new RELATIONSHIP();
                    entity.id = model.Id;
                    entity.relationship1 = model.Relationship;
                }
                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override Relationships TranslateToModel(RELATIONSHIP entity)
        {
            try
            {
                Relationships model = null;
                if(entity != null)
                {
                    model.Id = entity.id;
                    model.Relationship = entity.relationship1;
                }
                return model;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
