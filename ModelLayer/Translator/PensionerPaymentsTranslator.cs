﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Entity;
using ModelLayer.Model;

namespace ModelLayer.Translator
{
    public class PensionerPaymentsTranslator: TranslatorBase<PensionerPayments, PENSIONERS_PAYMENTS>
    {
        public override PENSIONERS_PAYMENTS TranslateToEntity(PensionerPayments model)
        {
            try
            {
                PENSIONERS_PAYMENTS entity = null;
                if(model != null)
                {
                    entity = new PENSIONERS_PAYMENTS();
                    entity.id = model.Id;
                    entity.payment_made_on = model.PaymentMadeOn;
                    entity.month = model.Month;
                    entity.year = model.Year;
                    entity.paymentIssued = model.PaymentIssued;
                }
                return entity;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override PensionerPayments TranslateToModel(PENSIONERS_PAYMENTS entity)
        {
            try
            {
                PensionerPayments model = null;
                if(entity != null)
                {
                    model = new PensionerPayments();
                    model.Id = entity.id;
                    model.PaymentMadeOn = (DateTime)entity.payment_made_on;
                    model.Month = entity.month;
                    model.Year = entity.year;
                    model.PaymentIssued = (int)entity.paymentIssued;
                }
                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}