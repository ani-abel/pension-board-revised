﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Entity;
using ModelLayer.Model;

namespace ModelLayer.Translator
{
    public class UserRolesTranslator: TranslatorBase<UserRoles, USER_ROLES>
    {
        public override USER_ROLES TranslateToEntity(UserRoles model)
        {
            try
            {
                USER_ROLES entity = null;
                if(model != null)
                { 
                    entity = new USER_ROLES();
                    entity.id = model.Id;
                    entity.role = model.Role;
                }
                return entity;
            }
            catch(Exception ex) { throw ex; }
        }

        public override UserRoles TranslateToModel(USER_ROLES entity)
        {
            try
            {
                UserRoles model = null;
                if(entity != null)
                {
                    model = new UserRoles();
                    model.Id = entity.id;
                    model.Role = entity.role;
                }
                return model;
            }
            catch(Exception ex) { throw ex; }
        }
    }

}