﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Entity;
using ModelLayer.Model;

namespace ModelLayer.Translator
{
    public class CivilServiceLevelTranslator: TranslatorBase<CivilServiceLevel, CIVIL_SERVICE_LEVEL>
    {
        public override CivilServiceLevel TranslateToModel(CIVIL_SERVICE_LEVEL entity)
        {
            try
            {
                CivilServiceLevel model = null;
                if(entity != null)
                {
                    model = new CivilServiceLevel();
                    model.Id = entity.id;
                    model.ServiceLevel = entity.service_level;
                }
                return model;
            }
            catch(Exception ex) { throw ex; }
        }

        public override CIVIL_SERVICE_LEVEL TranslateToEntity(CivilServiceLevel model)
        {
            try
            {
                CIVIL_SERVICE_LEVEL entity = null;
                if(model != null)
                {
                    entity = new CIVIL_SERVICE_LEVEL();
                    entity.id = model.Id;
                    entity.service_level = model.ServiceLevel;
                }
                return entity;
            }
            catch(Exception ex) { throw ex; }
        }
    }
}
