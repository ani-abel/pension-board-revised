﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Model;
using ModelLayer.Entity;

namespace ModelLayer.Translator
{
    public class PensionerEmploymentDataTranslator: TranslatorBase<PensionerEmploymentData, PENSIONER_EMPLOYMENT_DATA>
    {
        private PensionerTranslator PensionerTranslator;
        private RetirementReasonTranslator RetirementReasonTranslator;
        private JobPostsTranslator JobPostTranslator;
        private CivilServiceLevelTranslator CivilServiceLevelTranslator;

        public PensionerEmploymentDataTranslator()
        {
            PensionerTranslator = new PensionerTranslator();
            RetirementReasonTranslator = new RetirementReasonTranslator();
            JobPostTranslator = new JobPostsTranslator();
            CivilServiceLevelTranslator = new CivilServiceLevelTranslator();
        }

        public override PensionerEmploymentData TranslateToModel(PENSIONER_EMPLOYMENT_DATA entity)
        {
            try
            {
                PensionerEmploymentData model = null;
                if(entity != null)
                {
                    model = new PensionerEmploymentData();
                    model.Id = entity.id;
                    model.DateOfFirstEmployment = entity.date_of_first_appointment;
                    model.DateOfLastEmplyment = entity.date_of_last_appointment;
                    model.LastPost = JobPostTranslator.TranslateToModel(entity.JOB_POSTS);
                    model.GradeAsAtRetirement = CivilServiceLevelTranslator.TranslateToModel(entity.CIVIL_SERVICE_LEVEL);
                    model.Pensioner = PensionerTranslator.TranslateToModel(entity.PENSIONER);
                    model.RetirementReason = RetirementReasonTranslator.TranslateToModel(entity.RETIREMENT_REASON1);
                    model.LenghtOfSeason = entity.length_of_season;//This refers to how long the pensioner stayed in civil service
                }
                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override PENSIONER_EMPLOYMENT_DATA TranslateToEntity(PensionerEmploymentData model)
        {
            try
            {
                PENSIONER_EMPLOYMENT_DATA entity = null;
                if(model != null)
                {
                    entity = new PENSIONER_EMPLOYMENT_DATA();
                    entity.id = model.Id;
                    entity.date_of_first_appointment = model.DateOfFirstEmployment;
                    entity.date_of_last_appointment = model.DateOfLastEmplyment;
                    entity.last_post = model.LastPost.Id;
                    entity.grade_as_at_retirement = model.GradeAsAtRetirement.Id;
                    entity.retirement_reason = model.RetirementReason.Id;
                    entity.length_of_season = model.LenghtOfSeason;
                    entity.pensioner_id = model.Pensioner.Id;
                }
                return entity;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
