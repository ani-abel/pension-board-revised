﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Model;
using ModelLayer.Entity;

namespace ModelLayer.Translator
{
    public class StatesTranslator: TranslatorBase<States, STATE>
    {
        public override States TranslateToModel(STATE entity)
        {
            try
            {
                States model = null;
                if(entity != null)
                {
                    model = new States();
                    model.Id = entity.id;
                    model.State = entity.states;
                }
                return model;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override STATE TranslateToEntity(States model)
        {
            try
            {
                STATE entity = null;
                if(model != null)
                {
                    entity = new STATE();
                    entity.id = model.Id;
                    entity.states = model.State;
                }
                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}