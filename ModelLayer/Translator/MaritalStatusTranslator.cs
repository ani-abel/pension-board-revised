﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Model;
using ModelLayer.Entity;

namespace ModelLayer.Translator
{
    public class MaritalStatusTranslator: TranslatorBase<MaritalStatus, MARITAL_STATUS>
    {
        public override MaritalStatus TranslateToModel(MARITAL_STATUS entity)
        {
            try
            {
                MaritalStatus model = null;
                if(entity != null)
                {
                    model = new MaritalStatus();
                    model.Id = entity.id;
                    model.Status = entity.status;
                }
                return model;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override MARITAL_STATUS TranslateToEntity(MaritalStatus model)
        {
            try
            {
                MARITAL_STATUS entity = null;
                if(model != null)
                {
                    entity = new MARITAL_STATUS();
                    entity.id = model.Id;
                    entity.status = model.Status;
                }
                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}