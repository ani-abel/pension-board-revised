﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Entity;
using ModelLayer.Model;

namespace ModelLayer.Translator
{
    public class TitleTranslator: TranslatorBase<Titles, TITLE>
    {
        public override Titles TranslateToModel(TITLE entity)
        {
            try
            {
                Titles model = null;
                if(entity != null)
                {
                    model = new Titles();
                    model.Id = entity.id;
                    model.Title = entity.title1;
                }
                return model;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override TITLE TranslateToEntity(Titles model)
        {
            try
            {
                TITLE entity = null;
                if(model != null)
                {
                    entity = new TITLE();
                    entity.id = model.Id;
                    entity.title1 = model.Title;
                }
                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
