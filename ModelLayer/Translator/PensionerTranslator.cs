﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Entity;
using ModelLayer.Model;

namespace ModelLayer.Translator
{
    public class PensionerTranslator: TranslatorBase<Pensioner, PENSIONER>
    {
        private GenderTranslator GenderTranslator;
        private MaritalStatusTranslator MaritalStatusTranslator;
        private TitleTranslator TitleTranslator;

        public PensionerTranslator()
        {
            GenderTranslator = new GenderTranslator();
            MaritalStatusTranslator = new MaritalStatusTranslator();
            TitleTranslator = new TitleTranslator();
        }

        public override PENSIONER TranslateToEntity(Pensioner model)
        {
            try
            {
                PENSIONER entity = null;
                if(model != null)
                {
                    entity = new PENSIONER();
                    entity.id = model.Id;
                    entity.title = model.Title.Id;
                    entity.surname = model.Surname;
                    entity.other_names = model.OtherNames;
                    entity.gender = model.Gender.Id;
                    entity.marital_status = model.MaritalStatus.Id;
                    entity.dateCreated = model.DateCreated;
                    entity.maidian_name = model.MaidianName;
                    entity.spouse_name = model.SpouseName;
                    entity.deceased = model.IsDeceased;
                    entity.passport = model.Passport;

                }
                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override Pensioner TranslateToModel(PENSIONER entity)
        {
            try
            {
                Pensioner model = null;
                if(entity != null)
                {
                    model = new Pensioner();
                    model.Id = entity.id;
                    model.Title = TitleTranslator.Translate(entity.TITLE1);
                    model.Surname = entity.surname;
                    model.OtherNames = entity.other_names;
                    model.Gender = GenderTranslator.Translate(entity.GENDER1);
                    model.DateCreated = entity.dateCreated;
                    model.MaidianName = entity.maidian_name;
                    model.SpouseName = entity.spouse_name;
                    model.IsDeceased = entity.deceased;
                    model.MaritalStatus = MaritalStatusTranslator.Translate(entity.MARITAL_STATUS1);   
                    model.Passport = entity.passport;
                }
                return model;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}