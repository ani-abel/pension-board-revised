﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Model;
using ModelLayer.Entity;

namespace ModelLayer.Translator
{
    public class GenderTranslator: TranslatorBase<Gender, GENDER>
    {
        public override Gender TranslateToModel(GENDER entity)
        {
            try
            {
                Gender model = null;
                if(entity != null)
                {
                    model = new Gender();
                    model.Id = entity.id;
                    model.GenderType = entity.gender1;
                }
                return model;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override GENDER TranslateToEntity(Gender model)
        {
            try
            {
                GENDER entity = null;
                if (model != null){
                    entity = new GENDER();
                    entity.id = model.Id;
                    entity.gender1 = model.GenderType;
                }
                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
