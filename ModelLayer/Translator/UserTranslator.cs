﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Model;
using ModelLayer.Entity;


namespace ModelLayer.Translator
{
    public class UserTranslator: TranslatorBase<User, USER>
    {
        private UserRolesTranslator UserRolesTranslator;

        public UserTranslator()
        {
            UserRolesTranslator = new UserRolesTranslator();
        }

        public override USER TranslateToEntity(User model)
        {
            try
            {
                USER entity = null;
                if(model != null)
                {
                    entity = new USER();
                    entity.id = model.Id;
                    entity.username = model.Username;
                    entity.password = model.Password;
                    entity.role = model.Role.Id;
                }
                return entity;
            }
            catch (Exception ex) { throw ex; }
        }

        public override User TranslateToModel(USER entity)
        {
            try
            {
                User model = null;
                if(entity != null)
                {
                    model = new User();
                    model.Id = entity.id;
                    model.Username = entity.username;
                    model.Password = entity.password;
                    model.Role = UserRolesTranslator.TranslateToModel(entity.USER_ROLES);
                }
                return model;
            }
            catch(Exception ex) { throw ex; }
        }
    }
}
