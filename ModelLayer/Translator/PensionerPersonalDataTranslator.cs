﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Entity;
using ModelLayer.Model;

namespace ModelLayer.Translator
{
    public class PensionerPersonalDataTranslator: TranslatorBase<PensionerPersonalData, PENSIONER_PERSONAL_DATA>
    {
        private PensionerTranslator PensionerTranslator;
        private StatesTranslator StatesTranslator;

        public PensionerPersonalDataTranslator()
        {
            PensionerTranslator = new PensionerTranslator();
            StatesTranslator = new StatesTranslator();
        }

        public override PENSIONER_PERSONAL_DATA TranslateToEntity(PensionerPersonalData model)
        {
            try
            {
                PENSIONER_PERSONAL_DATA entity = null;
                if(model != null)
                {
                    entity = new PENSIONER_PERSONAL_DATA();
                    entity.id = model.Id;
                    entity.phone_no = model.PhoneNo;
                    entity.state = model.State.Id;
                    entity.home_town = model.Hometown;
                    entity.lga = model.Lga;
                    entity.nationality = model.Nationality;
                    entity.permanent_home_address = model.PermanentHomeAddress;
                    entity.dob = model.DateOfBirth;
                    entity.pensioner_id = model.Pensioner.Id;
                }
                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override PensionerPersonalData TranslateToModel(PENSIONER_PERSONAL_DATA entity)
        {
            try
            {
                PensionerPersonalData model = null;
                if(entity != null)
                {
                    model = new PensionerPersonalData();
                    model.Id = entity.id;
                    model.PhoneNo = entity.phone_no;
                    model.State = StatesTranslator.TranslateToModel(entity.STATE1);
                    model.Hometown = entity.home_town;
                    model.Lga = entity.lga;
                    model.Nationality = entity.nationality;
                    model.PermanentHomeAddress = entity.permanent_home_address;
                    model.DateOfBirth = entity.dob;
                    model.Pensioner = PensionerTranslator.TranslateToModel(entity.PENSIONER);
                }
                return model;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}