﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Model;
using ModelLayer.Entity;

namespace ModelLayer.Translator
{
    public class LgasTranslator: TranslatorBase<Lgas, LGA>
    {
        private StatesTranslator StatesTranslator;
        public LgasTranslator()
        {
            StatesTranslator = new StatesTranslator();
        }

        public override LGA TranslateToEntity(Lgas model)
        {
            try
            {
                LGA entity = null;
                if(model != null)
                {
                    entity = new LGA();
                    entity.id = model.Id;
                    entity.lga1 = model.Lga;
                    entity.state_id = model.State.Id;
                }
                return entity;
            }
            catch(Exception ex) { throw ex; }
        }

        public override Lgas TranslateToModel(LGA entity)
        {
            try
            {
                Lgas model = null;
                if(entity != null)
                {
                    model.Id = entity.id;
                    model.Lga = entity.lga1;
                    model.State = StatesTranslator.TranslateToModel(entity.STATE);
                }
                return model;
            }
            catch(Exception ex) { throw ex; }
        }
    }
}