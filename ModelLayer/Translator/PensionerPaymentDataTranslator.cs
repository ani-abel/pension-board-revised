﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Entity;
using ModelLayer.Model;

namespace ModelLayer.Translator
{
    public class PensionerPaymentDataTranslator: TranslatorBase<PensionerPaymentData, PENSIONER_PAYMENT_DATA>
    {
        private BankTranslator BankTranslator;
        private PensionerTranslator PensionerTranslator;

        public PensionerPaymentDataTranslator()
        {
            BankTranslator = new BankTranslator();
            PensionerTranslator = new PensionerTranslator();
        }

        public override PENSIONER_PAYMENT_DATA TranslateToEntity(PensionerPaymentData model)
        {
            try
            {
                PENSIONER_PAYMENT_DATA entity = null;
                if(model != null)
                {
                    entity = new PENSIONER_PAYMENT_DATA();
                    entity.id = model.Id;
                    entity.bank = model.Bank.Id;
                    entity.bank_branch = model.BankBranch;
                    entity.account_no = model.AccountNo;
                    entity.gross_pension = model.GrossPension;
                    entity.deductible = model.Deductible;
                    entity.net_pension = model.NetPension;
                    entity.pensioner_id = model.Pensioner.Id;
                }
                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override PensionerPaymentData TranslateToModel(PENSIONER_PAYMENT_DATA entity)
        {
            try
            {
                PensionerPaymentData model = null;
                if(entity != null)
                {
                    model = new PensionerPaymentData();
                    model.Id = entity.id;
                    model.Bank = BankTranslator.TranslateToModel(entity.BANK1);
                    model.BankBranch = entity.bank_branch;
                    model.AccountNo = entity.account_no;
                    model.GrossPension = entity.gross_pension;
                    model.Deductible = entity.deductible;
                    model.NetPension = (long)entity.net_pension;
                    model.Pensioner = PensionerTranslator.TranslateToModel(entity.PENSIONER);
                }
                return model;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
