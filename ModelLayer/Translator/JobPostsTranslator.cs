﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Model;
using ModelLayer.Entity;


namespace ModelLayer.Translator
{
    public class JobPostsTranslator: TranslatorBase<JobPosts, JOB_POSTS>
    {
        public override JobPosts TranslateToModel(JOB_POSTS entity)
        {
            try
            {
                JobPosts model = null;
                if(entity != null)
                {
                    model = new JobPosts();
                    model.Id = entity.id;
                    model.JobPosting = entity.job_posting;
                }
                return model;
            }
            catch(Exception ex) { throw ex; }
        }

        public override JOB_POSTS TranslateToEntity(JobPosts model)
        {
            try
            {
                JOB_POSTS entity = null;
                if(model != null)
                {
                    entity = new JOB_POSTS();
                    entity.id = model.Id;
                    entity.job_posting = model.JobPosting;
                }
                return entity;
            }
            catch(Exception ex) { throw ex; }
        }

    }
}