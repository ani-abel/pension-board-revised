﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Entity;
using ModelLayer.Model;

namespace ModelLayer.Translator
{
    public class PensionerNextOfKinDataTranslator: TranslatorBase<PensionerNextOfKinData, PENSIONER_NEXT_OF_KIN_DATA>
    {
        private RelationshipTranslator RelationshipTranslator;
        private GenderTranslator GenderTranslator;
        private PensionerTranslator PensionerTranslator;

        public PensionerNextOfKinDataTranslator()
        {
            PensionerTranslator = new PensionerTranslator();
            RelationshipTranslator = new RelationshipTranslator();
            GenderTranslator = new GenderTranslator();
        }
         
        public override PensionerNextOfKinData TranslateToModel(PENSIONER_NEXT_OF_KIN_DATA entity)
        {
            try
            {
                PensionerNextOfKinData model = null;
                if(entity != null)
                {
                    model = new PensionerNextOfKinData();
                    model.Id = entity.id;
                    model.passport = entity.passport;
                    model.Phoneno = entity.phone_no;
                    model.Relationship = RelationshipTranslator.TranslateToModel(entity.RELATIONSHIP1);
                    model.Gender = GenderTranslator.TranslateToModel(entity.GENDER1);
                    model.PermanentAddress = entity.permanent_address;
                    model.IsReceivingPension = entity.isReceiving_pension;
                    model.Surname = entity.surname;
                    model.OtherNames = entity.other_names;
                    model.Pensioner = PensionerTranslator.TranslateToModel(entity.PENSIONER);
                }
                return model;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override PENSIONER_NEXT_OF_KIN_DATA TranslateToEntity(PensionerNextOfKinData model)
        {
            try
            {
                PENSIONER_NEXT_OF_KIN_DATA entity = null;
                if(model != null)
                {
                    entity = new PENSIONER_NEXT_OF_KIN_DATA();
                    entity.id = model.Id;
                    entity.passport = model.passport;
                    entity.phone_no = model.Phoneno;
                    entity.relationship = model.Relationship.Id;
                    entity.gender = model.Gender.Id;
                    entity.permanent_address = model.PermanentAddress;
                    entity.isReceiving_pension = model.IsReceivingPension;//An integer that represensts boolean, 0 for false
                    entity.surname = model.Surname;
                    entity.other_names = model.OtherNames;
                    entity.pensioner_id = model.Pensioner.Id;

                }
                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
