﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Model;
using ModelLayer.Entity;

namespace ModelLayer.Translator
{
    public class BankTranslator : TranslatorBase<Bank, BANK>
    {
        public override Bank TranslateToModel(BANK entity)
        {
            try
            {
                Bank model = null;
                if(entity != null)
                {
                    model = new Bank();
                    model.Id = entity.id;
                    model.BankName = entity.bank1;
                }
                return model;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override BANK TranslateToEntity(Bank model)
        {
            try
            {
                BANK entity = null;
                if(model != null)
                {
                    entity = new BANK();
                    entity.id = model.Id;
                    entity.bank1 = model.BankName;
                }
                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
