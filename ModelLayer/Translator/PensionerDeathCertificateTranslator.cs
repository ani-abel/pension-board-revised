﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Entity;
using ModelLayer.Model;

namespace ModelLayer.Translator
{
    public class PensionerDeathCertificateTranslator: TranslatorBase<PensionerDeathCertificate, PENSIONER_DEATH_CERTIFICATE>
    {
        private PensionerTranslator PensionerTranslator;

        public PensionerDeathCertificateTranslator()
        {
            PensionerTranslator = new PensionerTranslator();
        }

        public override PENSIONER_DEATH_CERTIFICATE TranslateToEntity(PensionerDeathCertificate model)
        {
            try
            {
                PENSIONER_DEATH_CERTIFICATE entity = null;
                if (model != null)
                {
                    entity = new PENSIONER_DEATH_CERTIFICATE();
                    entity.id = model.Id;
                    entity.pensioner_id = model.Pensioner.Id;
                    entity.isValidated = model.IsValidated;
                    entity.date_of_death = model.DateOfDeath;
                    entity.death_certificate = model.DeathCertificate;
                    entity.date_validated = model.DateValidated;
                }
                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override PensionerDeathCertificate TranslateToModel(PENSIONER_DEATH_CERTIFICATE entity)
        {
            try
            {
                PensionerDeathCertificate model = null;
                if(entity != null)
                {
                    model = new PensionerDeathCertificate();
                    model.Id = entity.id;
                    model.Pensioner = PensionerTranslator.TranslateToModel(entity.PENSIONER);
                    model.IsValidated = entity.isValidated;
                    model.DateOfDeath = entity.date_of_death;
                    model.DeathCertificate = entity.death_certificate;
                    model.IsValidated = entity.isValidated;
                }
                return model;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
