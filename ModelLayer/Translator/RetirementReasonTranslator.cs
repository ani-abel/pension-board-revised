﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModelLayer.Entity;
using ModelLayer.Model;

namespace ModelLayer.Translator
{
    public class RetirementReasonTranslator: TranslatorBase<RetirementReason, RETIREMENT_REASON>
    {
        public override RETIREMENT_REASON TranslateToEntity(RetirementReason model)
        {
            try
            {
                RETIREMENT_REASON entity = null;
                if(model != null)
                {
                    entity = new RETIREMENT_REASON();
                    entity.id = model.Id;
                    entity.reason = model.Reason;
                }
                return entity;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override RetirementReason TranslateToModel(RETIREMENT_REASON entity)
        {
            try
            {
                RetirementReason model = null;
                if(entity != null)
                {
                    model = new RetirementReason();
                    model.Id = entity.id;
                    model.Reason = entity.reason;
                }
                return model;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
